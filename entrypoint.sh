#! /bin/sh

set -e

APP_ENV="${APP_ENV:-prod}"
APP_LMS="${APP_LMS:-canvas}"
if [ -z "$BASE_URL" ]
then
    printf '%s: error: BASE_URL not set\n' "${0##*/}" >&2
    exit 1
elif [ -z "$DATABASE_URL" ]
then
    printf '%s: error: DATABASE_URL not set\n' "${0##*/}" >&2
    exit 1
fi
cd "/opt/udoit"
php81 bin/console "doctrine:migrations:migrate" --no-interaction
php81 bin/console "cache:warmup" --env="prod"
cd "/tmp"
cp "/etc/nginx/nginx.conf" "./nginx.conf"
_WPROCS="$(getconf _NPROCESSORS_ONLN)"
_WCONNS="$((_WPROCS*2048))"
sed -e "s/^\(worker_processes \)1;$/\1$_WPROCS;/" \
    -e "s/^\( *worker_connections \)2048;$/\1$_WCONNS;/" \
    "./nginx.conf" >"/etc/nginx/nginx.conf"
rm "./nginx.conf"

exec supervisord
